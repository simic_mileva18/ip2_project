update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='Orf1ab protein';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase' and accession_prot='AAL57305.1';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase' and accession_prot='AAK83365.1';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf1ab polyprotein';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein orf1ab';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF1ab';
update bcov.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein 1ab';
  
-- u grupi BCOV sa početkom ON7929xx.y su orf1a i orf1b  razdvojeni sa 4 nukleotida, pri čemu je orf1b označen kao ORF1ab!!!!
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='Orf1a protein';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='replicase';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='RNA polymerase 1a';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a polyprotein';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='replicase polyprotein 1a';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein orf1a';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein 1a';
update bcov.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF1a';
	  
update bcov.metapodaci_proteina_temp set protein='ORF1b polyprotein' where protein='RNA polymerase 1b';
update bcov.metapodaci_proteina_temp set protein='ORF1b polyprotein' where protein='replicase polyprotein 1ab';
update bcov.metapodaci_proteina_temp set protein='ORF1b polyprotein' where protein='RNA dependent RNA polymerase';

update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike protein';
update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike glycoprotein';
update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike protein precursor';
update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike structural protein';
update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S';
update bcov.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S protein';

update bcov.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocapsid protein';
update bcov.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocaspid protein';
update bcov.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleoprotein';
update bcov.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N';
update bcov.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N protein';

update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemmaglutinin-esterase';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinn-esterase';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin esterase';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='HE';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase precursor';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase protein';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='HE protein';
update bcov.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase envelope glycoprotein';

update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='32 kDa protein';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='32 kD non-structural protein';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='nonstructural protein 2a';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='32KDa protein product';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='nonstructural protein' and accession_prot='ANJ04972.1';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='nonstructural protein' and accession_prot='QOV05154.1';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='nonstructural protein' and accession_prot='QOV05167.1';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='non-structural protein' and accession_prot='ABG89287.1';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='non-structural protein' and accession_prot='UUX91087.1';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein='32 kDa protein product';
update bcov.metapodaci_proteina_temp set protein='32 kDa non-structural protein' where protein like 'NS2%';

update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane protein';
update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M protein';
update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='multispanning envelope protein';
update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane multispanning envelope protein';
update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M';
update bcov.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='matrix protein';

update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein='12.7 kDa protein';
update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein='12.7 kD non-structural protein';
update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein='12.7 kDa non structural protein';
update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein='12.7 kDa nonstructural protein';
update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein like 'NS12%';
update bcov.metapodaci_proteina_temp set protein='12.7 kDa non-structural protein' where protein='non-structural protein' and accession_prot='UUX91092.1';

update bcov.metapodaci_proteina_temp set protein='envelope protein' where protein='small envelope protein';
update bcov.metapodaci_proteina_temp set protein='envelope protein' where protein='small envelpoe protein';
update bcov.metapodaci_proteina_temp set protein='envelope protein' where protein like '%small membrane protein%';
update bcov.metapodaci_proteina_temp set protein='envelope protein' where protein='E';
update bcov.metapodaci_proteina_temp set protein='envelope protein' where protein='E protein';

update bcov.metapodaci_proteina_temp set protein='4.9 kDa non-structural protein' where protein='4.9 kDa protein';
update bcov.metapodaci_proteina_temp set protein='4.9 kDa non-structural protein' where protein='4.9 kD non-structural protein';
update bcov.metapodaci_proteina_temp set protein='4.9 kDa non-structural protein' where protein='4.9 kDa nonstructural protein';
update bcov.metapodaci_proteina_temp set protein='4.9 kDa non-structural protein' where protein='4.9 kDa non structural protein';
update bcov.metapodaci_proteina_temp set protein='4.9 kDa non-structural protein' where protein='NS4.9';

update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kDa protein';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kDA protein';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='NS4.8';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kD non-structural protein';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kDa non-structural protein';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kDa nonstructural protein';
update bcov.metapodaci_proteina_temp set protein='4.8 kDa non-structural protein' where protein='4.8 kDa non structural protein';

 
