update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF1a';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF1A polyprotein';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='1A polyprotein';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF 1a';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF1a protein';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a protein';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein ORF1a';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein 1a';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1ab' and accession_prot='AGN72638.1';
update mers.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein orf1a';


update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein 1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='1AB polyprotein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF1AB polyprotein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF1ab protein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf1ab protein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf1b protein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein ORF1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein orf1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF 1ab';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase polyprotein';
update mers.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='ORF1ab polyprotein';

update mers.metapodaci_proteina_temp set protein='ORF1b polyprotein' where protein='ORF1b';
update mers.metapodaci_proteina_temp set protein='ORF1b polyprotein' where protein='orf1b';

update mers.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike protein';
update mers.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S protein';
update mers.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S';
update mers.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike glycoprotein';
update mers.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike';

update mers.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocapsid protein';
update mers.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N protein';
update mers.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N';
update mers.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleoprotein';
update mers.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocapsid';


update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='ORF5';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='orf5';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='orf5 protein';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='NS3D protein';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='NS3d protein';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='NS5 protein';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='ORF 5';
update mers.metapodaci_proteina_temp set protein='ORF5 protein' where protein='ORF4b' and (accession_prot='AVV62540.1' or 
                                                                                                         accession_prot='AWM99586.1' or
																										 accession_prot='AWU59325.1' or 
																										 accession_prot='AVV61904.1');
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='non-structural protein 5';

update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='orf4b';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='ORF4b';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='orf4b protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='NS3C protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='NS3c protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='NS4B protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='NS4b protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='ORF 4b';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='ORF4b protein';
update mers.metapodaci_proteina_temp set protein='ORF4b protein' where protein='non-structural protein 4b';

update mers.metapodaci_proteina_temp set protein='envelope protein' where protein='E';
update mers.metapodaci_proteina_temp set protein='envelope protein' where protein='E protein';
update mers.metapodaci_proteina_temp set protein='envelope protein' where protein='envelope';
update mers.metapodaci_proteina_temp set protein='envelope protein' where protein='small envelope protein';
update mers.metapodaci_proteina_temp set protein='envelope protein' where protein='membrane protein'  and accession_prot='ALT66876.1';

update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane protein';
update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M';
update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M protein';
update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M protein|';
update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane';
update mers.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='envelope protein'  and accession_prot='ALT66875.1';


update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='ORF3';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='orf3';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='orf3 protein';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='NS3 protein';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='NS3a protein';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='ORF 3';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='NS3A protein';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='non-structural protein 3a';
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='ORF4a'  and (accession_prot='QOU08510.1' or 
                                                                                                    accession_prot='AVN89454.1' or
																									accession_prot='QOU08521.1' or 
																									accession_prot='AWH65945.1' or 
																									accession_prot='AWH65956.1' or 
																									accession_prot='USF97416.1' or 
																									accession_prot='AVV62528.1' or 
																									accession_prot='AHY61339.1' or 
																									accession_prot='AVV62539.1' or																										  
																									accession_prot='QOU08532.1');
												  
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='ORF4a protein'  and (accession_prot='AUM60016.1' or 
                                                                                                                  accession_prot='AUM60026.1' or
																						         				  accession_prot='USL83013.1');
update mers.metapodaci_proteina_temp set protein='ORF3 protein' where protein='NS4A protein'  and (accession_prot='ARQ84737.1' or 
                                                                                                                 accession_prot='ARQ84748.1' or
                                                                                                                 accession_prot='ARQ84759.1' or																												 
																						             		     accession_prot='ARQ84770.1');





update mers.metapodaci_proteina_temp set protein='ORF8b protein' where protein='ORF8b';
update mers.metapodaci_proteina_temp set protein='ORF8b protein' where protein='ORF 8b';
update mers.metapodaci_proteina_temp set protein='ORF8b protein' where protein='orf8b';
update mers.metapodaci_proteina_temp set protein='ORF8b protein' where protein='orf8b protein';



update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='orf4a';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='ORF4a';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='ORF 4a';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='NS3b protein';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='NS3B protein';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='NS4A protein';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='NS4a protein';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='orf4a protein';
update mers.metapodaci_proteina_temp set protein='ORF4a protein' where protein='non-structural protein 4a';	   
	   