
update sars_cov_1.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein orf1ab';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase 1AB';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='polyprotein orf1a';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a polyprotein (pp1a)';
update sars_cov_1.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike glycoprotein';
update sars_cov_1.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike glycoprotein precursor';
update sars_cov_1.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='E2 glycoprotein precursor';
update sars_cov_1.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocapsid protein';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF3a protein' where protein='Orf3';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF3a protein' where protein='hypothetical protein sars3a';


update sars_cov_1.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane protein';
update sars_cov_1.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane glycoprotein M';
update sars_cov_1.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='matrix protein';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF7a protein' where protein='hypothetical protein sars7a';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF7a protein' where protein='Orf8' and accession_prot='AAP41043.1';

update sars_cov_1.metapodaci_proteina_temp set protein='envelope protein' where protein='protein E';
update sars_cov_1.metapodaci_proteina_temp set protein='envelope protein' where protein='small envelope protein';
update sars_cov_1.metapodaci_proteina_temp set protein='envelope protein' where protein='small envelope E protein';



update sars_cov_1.metapodaci_proteina_temp set protein='ORF8 protein' where protein='hypothetical protein sars8b';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF8 protein' where protein='Orf11' and accession_prot='AAP41046.1';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF8 protein' where protein='ORF8b protein';



update sars_cov_1.metapodaci_proteina_temp set protein='ORF6 protein' where protein='hypothetical protein sars6';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF6 protein' where protein='Orf7' and accession_prot='AAP41042.1';



update sars_cov_1.metapodaci_proteina_temp set protein='ORF10 protein' where protein='Orf10';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF10 protein' where protein='ORF8a protein' and accession_prot='YP_009825059.1';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF10 protein' where protein='hypothetical protein sars8a' and accession_prot='NP_849176.1';

update sars_cov_1.metapodaci_proteina_temp set protein='ORF7b protein' where protein='hypothetical protein sars7b';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF7b protein' where protein='non-structural protein NS7b';
update sars_cov_1.metapodaci_proteina_temp set protein='ORF7b protein' where protein='Orf9' and accession_prot='AAP41044.1';