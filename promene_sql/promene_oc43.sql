
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleocapsid protein';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='nucleoprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N protein';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='N';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='I';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='unknown';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein' where protein='protein I';
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein 621nuc' where protein='nucleocapsid phosphoprotein' and duzina_aa=207;
-- aa=207 ---> dužina nukleotida je 621
update hcov_oc43.metapodaci_proteina_temp set protein='nucleocapsid phosphoprotein 1341nuc' where protein='nucleocapsid phosphoprotein' and duzina_aa>207;
-- aa>207 ---> dužina nukleotida je veća od 621

update hcov_oc43.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='membrane protein';
update hcov_oc43.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M protein';
update hcov_oc43.metapodaci_proteina_temp set protein='membrane glycoprotein' where protein='M';


update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase';
update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-neuraminidase';
update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin esterase';
update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='hemagglutinin-esterase protein';
update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='HE';
update hcov_oc43.metapodaci_proteina_temp set protein='hemagglutinin-esterase glycoprotein' where protein='HE protein';

update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike glycoprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike surface glycoprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='spike protein';
update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S';
update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S protein';
update hcov_oc43.metapodaci_proteina_temp set protein='surface glycoprotein' where protein='S glycoprotein';


update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase polyprotein 1ab';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='replicase polyprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf1ab polyprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='polyprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='Pp1ab';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='orf 1ab';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1ab polyprotein' where protein='Orf1ab';

update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='replicase polyprotein 1a';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a protein';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='Pp1a';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf 1a';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='orf1a polyprotein';
update hcov_oc43.metapodaci_proteina_temp set protein='ORF1a polyprotein' where protein='ORF1a protein';

update hcov_oc43.metapodaci_proteina_temp set protein='envelope protein' where protein='E protein';
update hcov_oc43.metapodaci_proteina_temp set protein='envelope protein' where protein='E';
update hcov_oc43.metapodaci_proteina_temp set protein='envelope protein' where protein='NS3';
update hcov_oc43.metapodaci_proteina_temp set protein='envelope protein' where protein='NS3 protein';
update hcov_oc43.metapodaci_proteina_temp set protein='envelope protein' where protein='envelope small membrane protein';

update hcov_oc43.metapodaci_proteina_temp set protein='non-structural protein 2a' where  duzina_aa=278; -- dužina nuc=278*3=834
update hcov_oc43.metapodaci_proteina_temp set protein='non-struc prot duz 327 -4,5a,12.9,etc' where duzina_aa=109 and accession_prot not in ('YP_009924327.1','YP_009555252.1');
-- dužina nuc=109*3=327
